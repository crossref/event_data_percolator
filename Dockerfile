# Event Data Percolator
# Production build of Crossref Event Data Percolator

FROM clojure:openjdk-11-lein-2.9.3

COPY target/uberjar/*-standalone.jar ./percolator.jar

ENTRYPOINT ["java", "-jar", "percolator.jar"]
