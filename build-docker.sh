# This will build the Clojure project and package it into an image called percolator:latest .

lein clean
lein uberjar
docker build . -t percolator
