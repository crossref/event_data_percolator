(ns event-data-percolator.util.util
  (:require [clojure.java.io :refer [resource]]
            [clojure.string :refer [trim]]))

(def percolator-version
  "Retrieve version either from project file (when running in lein run)
   or from a VERSION.txt file that's injected at CI build time."
  (or
   (System/getProperty "event-data-percolator.version")
   (trim (-> 
          "VERSION.txt"
          resource
          slurp))))