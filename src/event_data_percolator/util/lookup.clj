(ns event-data-percolator.util.lookup
  "Look up content types."
  (:require [crossref.util.doi :as cr-doi]
            [org.httpkit.client :as http]
            [event-data-percolator.util.web :as web]
            [event-data-percolator.util.util :as util]
            [config.core :refer [env]]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [robert.bruce :refer [try-try-again]]
            [clojure.string :as string]
            [throttler.core :refer [throttle-fn]])
  (:import [java.net URLEncoder]))

(def mailto (or (:percolator-mailto env) "event-data-issues@crossref.org"))

(def user-agent
  (str "Percolator/"
       util/percolator-version
       " "
       "(https://www.crossref.org/services/event-data/; mailto:" mailto ")"))

(defn- get-rate-value
  [property default]
  (or
   (when-let [rate (property env)]
     (Integer/parseInt rate))
   default))

(def crossref-endpoint#
  (throttle-fn
   (fn [safe-doi] (str "https://api.crossref.org/v1/works/" safe-doi "?mailto=" mailto))
   (get-rate-value :percolator-rest-api-rate-limit 30) :second))

(def datacite-endpoint#
  (throttle-fn
   (fn [safe-doi] (str "https://api.datacite.org/dois/" safe-doi))
   (get-rate-value :percolator-datacite-rate-limit 9) :second))

(defn http-get
  [url]
  (deref (http/get url {:as :text
                        :client web/sni-client
                        :user-agent user-agent})))

(def get-ra-api#
  "Get the Registration Agency from the DOI RA API. 
   Return :crossref :datacite or nil."
  (throttle-fn
   (fn
     [non-url-normalized-doi]
     (when non-url-normalized-doi
       (try
         (-> non-url-normalized-doi
             (URLEncoder/encode "UTF-8")
             (#(str "https://doi.org/doiRA/" %))
             http-get
             :body
             (json/read-str :key-fn keyword)
             first
             :RA
             (or "")
             string/lower-case
             {"datacite" :datacite
              "crossref" :crossref})

      ; Not found, or invalid data, return nil.
         (catch Exception ex (do (log/error ex) nil)))))
   (get-rate-value :percolator-doi-org-rate-limit 60) :second))

(defn- get-work-api
  "Get the work metadata from the Crossref or DataCite API."
  [non-url-normalized-doi ra]
  ; We might get nils.
  (when non-url-normalized-doi
    (try
      (let [ra (or ra (get-ra-api# non-url-normalized-doi))
            safe-doi (URLEncoder/encode non-url-normalized-doi "UTF-8")
            url (condp = ra
                  :crossref (crossref-endpoint# safe-doi)
                  :datacite (datacite-endpoint# safe-doi)
                  nil)

            response (try-try-again
                      {:sleep 10000 :tries 2}
                        ; Only retry on genuine exceptions. 404 etc won't be fixed by retrying.
                      #(when url (http-get url)))
            body (when (= 200 (:status response)) (-> response :body (json/read-str :key-fn keyword)))
            work-type (condp = ra
                        :crossref (-> body :message :type)
                        :datacite (-> body :data :attributes :types :resourceTypeGeneral)
                        nil)]
      ; If we couldn't discover the RA, then this isn't a real DOI. 
      ; Return nil so this doens't get cached (could produce a false-negative in future).

        (when (and ra work-type)
          {:content-type work-type :doi non-url-normalized-doi}))
      (catch Exception ex        
        (log/error "Failed to retrieve metadata for DOI" non-url-normalized-doi "error:" (str ex))
        nil))))

(defn- get-for-doi
  [doi ra]
  (let [normalized-doi (when (cr-doi/well-formed doi)
                         (cr-doi/non-url-doi doi))]
    (get-work-api normalized-doi ra)))

(defn get-work-type-for-doi
  [doi ra]
  (-> (get-for-doi doi ra) :content-type))

(defn doi-ra [doi]
  (get-ra-api# (when (cr-doi/well-formed doi) (cr-doi/non-url-doi doi))))