(ns event-data-percolator.util.lookup-test
  (:require [clojure.test :refer :all]
            [event-data-percolator.util.util :as util]))

(deftest ^:unit percolator-version
  (testing "percolator-version returns the version from VERSION.txt"
    (is (= util/percolator-version "0.1.0-SNAPSHOT"))))