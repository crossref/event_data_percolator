(ns event-data-percolator.util.lookup-test
  (:require [clojure.test :refer :all]
            [clojure.data.json :as json]
            [event-data-percolator.util.lookup :as lookup]
            [clj-http.client :as client]))

(defn doi-ra-crossref
  [_]
  {:status 200 :body (json/write-str [{"RA" "Crossref"}])})

(defn doi-ra-datacite
  [_]
  {:status 200 :body (json/write-str [{"RA" "Datacite"}])})

(defn work-type-id-crossref-article
  [_]
  {:status 200 :body (json/write-str {"message" {"type" "journal-article"}})})

(defn work-type-id-crossref-dataset
  [_]
  {:status 200 :body (json/write-str {"message" {"type" "dataset"}})})

(defn work-type-id-datacite-article
  [_]
  {:status 200 :body (json/write-str {"data" {"attributes" {"types" {"resourceTypeGeneral" "Text"}}}})})

(defn work-type-id-datacite-dataset
  [_]
  {:status 200 :body (json/write-str {"data" {"attributes" {"types" {"resourceTypeGeneral" "Dataset"}}}})})

(deftest ^:component get-ra-api#
  (testing "get-ra-api returns correct registration agency"
    (with-redefs [client/get doi-ra-crossref]
      (is (= (lookup/get-ra-api# "10.2105/AJPH.2014.302190")
             :crossref)))
    (with-redefs [client/get doi-ra-datacite]
      (is (= (lookup/get-ra-api# "10.5061/dryad.fn2z34trn")
             :datacite)))))

(deftest ^:unit work-type-id-from-doi
  (testing "Object work-type-id can be fetched for all supported sources (Crossref, Datacite)"
    (with-redefs [lookup/http-get work-type-id-crossref-article]
      (is (= (lookup/get-work-type-for-doi "10.2105/AJPH.2014.302190" :crossref)
             "journal-article")))
    (with-redefs [lookup/http-get work-type-id-crossref-dataset]
      (is (= (lookup/get-work-type-for-doi "10.7287/peerj.preprints.26962v1/supp-1" :crossref)
             "dataset")))
    (with-redefs [lookup/http-get work-type-id-datacite-article]
      (is (= (lookup/get-work-type-for-doi "10.1001/archpedi.159.5.470" :datacite)
             "Text")))
    (with-redefs [lookup/http-get work-type-id-datacite-dataset]
      (is (= (lookup/get-work-type-for-doi "10.5061/dryad.fn2z34trn" :datacite)
             "Dataset")))))